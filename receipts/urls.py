from django.urls import path
from receipts.views import (
    show_receipts_list,
    create_receipt_list,
    show_accounts_list,
    show_categories_list,
    create_expense_category,
    create_account,
)

urlpatterns = [
    path("", show_receipts_list, name="home"),
    path("create/", create_receipt_list, name="create_receipt"),
    path("categories/", show_categories_list, name="category_list"),
    path("accounts/", show_accounts_list, name="account_list"),
    path(
        "categories/create/", create_expense_category, name="create_category"
    ),
    path("accounts/create/", create_account, name="create_account"),
]
