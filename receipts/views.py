from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptListForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def show_receipts_list(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts_list": receipts_list}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt_list(request):
    if request.method == "POST":
        form = ReceiptListForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")

    else:
        form = ReceiptListForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def show_categories_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories_list": categories}
    return render(request, "receipts/categories.html", context)


@login_required
def show_accounts_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts_list": accounts}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
        return redirect("category_list")

    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/expense.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
        return redirect("account_list")

    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/account.html", context)
